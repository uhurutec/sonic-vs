# SONiC Switch Images

At the time of writing, the only purpose of this repository is to
provide a checksum for
<https://github.com/Broadcom/sonic-VirtualSwitch/releases/download/3.1.2/sonic-vs-3.1.2.img.gz>